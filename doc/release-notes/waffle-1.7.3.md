
The Waffle bugfix release 1.7.3 is now available.

What is new in this release:
  - cgl: fix warnings and compilation issues
  - meson: drop lib prefix for waffle-1.dll

The source is available at:

  https://waffle.freedesktop.org/files/release/waffle-1.7.3/waffle-1.7.3.tar.xz

You can also get the current source directly from the git
repository. See https://waffle.freedesktop.org for details.

----------------------------------------------------------------

Changes since 1.7.2:

Emil Velikov (3):
      Use a static waffle.def
      meson: drop the libwaffle-1.dll lib prefix on windows
      waffle: Bump version to 1.7.3

Yurii Kolesnykov (2):
      Fix build on macOS by fixing a typo
      Add cflag to fix macOS build

