variables:
  FDO_UPSTREAM_REPO: mesa/waffle
  FDO_DISTRIBUTION_TAG: '2022-09-13'
  FDO_DISTRIBUTION_VERSION: 'bullseye-slim'

include:
  - project: 'freedesktop/ci-templates'
    ref: &ci-templates-sha 52dd4a94044449c8481d18dcdc221a3c636366d2
    file: '/templates/debian.yml'

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
    - if: $CI_PIPELINE_SOURCE == 'push'

stages:
  - container
  - style-check
  - build
  - www

# When to automatically run the CI
.ci-run-policy:
  retry:
    max: 2
    when:
      - runner_system_failure
  # Cancel CI run if a newer commit is pushed to the same branch
  interruptible: true

container:
  stage: container
  extends:
    - .fdo.container-build@debian
    - .ci-run-policy
  variables:
    # no need to pull the whole repo to build the container image
    GIT_STRATEGY: none

    # clang-format requires git, yet it's missing in the packaging
    FDO_DISTRIBUTION_PACKAGES: >
      bash-completion
      ca-certificates
      clang-format
      cmake
      docbook-xsl
      docbook-xml
      g++
      gcc
      git
      libcmocka-dev
      libdrm-dev
      libegl-dev
      libgbm-dev
      libgl-dev
      libgles-dev
      libglx-dev
      libwayland-dev
      libx11-xcb-dev
      make
      meson
      mingw-w64
      pkg-config
      wayland-protocols
      xauth
      xsltproc
      xvfb

clang-format:
  extends:
    - .ci-run-policy
    - .fdo.container-build@debian
  stage: style-check
  variables:
    GIT_DEPTH: 100
  image: "$CI_REGISTRY_IMAGE/debian/$FDO_DISTRIBUTION_VERSION:$FDO_DISTRIBUTION_TAG"
  needs:
    - container
  script:
    - .gitlab-ci/clang-format.sh

# BUILD
.build:
  extends:
    - .ci-run-policy
    - .fdo.container-build@debian
  stage: build
  variables:
    GIT_DEPTH: 100
  image: "$CI_REGISTRY_IMAGE/debian/$FDO_DISTRIBUTION_VERSION:$FDO_DISTRIBUTION_TAG"
  needs:
    - container


meson:
  extends:
    - .build
  script:
    - bash .gitlab-ci/build-meson.sh x11
    - bash .gitlab-ci/build-meson.sh wayland
    - bash .gitlab-ci/build-meson.sh gbm
    - bash .gitlab-ci/build-meson.sh surfaceless
  artifacts:
    when: on_failure
    paths:
      - _build/meson-logs/*.txt

cmake:
  extends:
    - .build
  script:
    - cmake
        -S . -B _build
        -DCMAKE_INSTALL_PREFIX=`pwd`/install
        -DCMAKE_BUILD_TYPE=Debug
        -Dwaffle_enable_deprecated_build=1
        -Dwaffle_has_gbm=1
        -Dwaffle_has_glx=1
        -Dwaffle_has_x11_egl=1
        -Dwaffle_has_wayland=1
        -Dwaffle_has_surfaceless=1
        -Dwaffle_build_manpages=1
        -Dwaffle_build_htmldocs=1
        -Dwaffle_build_examples=1
    - make -C _build -j4
    - make -C _build check
    - make -C _build install

cmake-mingw:
  extends:
    - .build
  script:
    - .gitlab-ci/build-cmake-mingw.sh
  # TODO: Only create artifacts on certain builds.  See also:
  # - https://docs.gitlab.com/ee/ci/yaml/README.html#artifactspaths
  # - https://docs.gitlab.com/ee/ci/yaml/README.html#complete-example-for-release
  artifacts:
    paths:
      - publish/*/waffle-*.zip

pages:
  stage: www
  image: alpine
  script:
  - apk add --no-cache git git-lfs
  - git lfs install
  - mkdir public
  - git checkout origin/tarballs
  - git log --oneline -1
  - cp -r files public
  - git checkout origin/master
  - git log --oneline -1
  - cp -r www/* public
  - cp -r doc/release-notes public/files
  artifacts:
    paths:
    - public
  only:
    refs:
      - master
    changes:
      - .gitlab-ci.yml
      - doc/release-notes/**/*
      - www/**/*

